import React from 'react';

// Component imports
import InputField from './components/InputField';
import ToDoTable from './components/ToDoTable';

// Style imports
import './App.css';


class App extends React.Component {
  constructor() {
    super();
    this.state = {
      tasks: []
    };
  }

  addTask = (task) => {
    const tasks = this.state.tasks;
    tasks.push(task);
    this.setState({
      tasks: tasks
    });
    console.log(tasks);
  }

  render() {
    return (
      <div className="App">
        <div className="header-text">ToDo:</div>
        <InputField addTask={this.addTask} />
        <ToDoTable tasks={this.state.tasks} />
      </div>
    );
  }
}

export default App;
