import React from 'react';
import ToDoTask from './ToDoTask';

// Style imports
import './ToDoTable.css';


class ToDoTable extends React.Component {
  render() {
    return (
      <div className="ToDoTable">
        {this.props.tasks.map((value, index) => {
          return <ToDoTask key={index} value={value} />;
        })}
      </div>
    );
  }
}

export default ToDoTable;