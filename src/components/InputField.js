import React from 'react';

// Style imports
import './InputField.css';


class InputField extends React.Component {
  submitHandler = (event) => {
    event.preventDefault();
    const element = document.getElementById('task-input');
    this.props.addTask(element.value);
    element.value = '';
  }

  render() {
    return (
      <form className="InputField" onSubmit={this.submitHandler}>
        <input id="task-input" className="input" />
        <input id="submit-button" type="submit" value="Add task" />
      </form>
    );
  }
}

export default InputField;