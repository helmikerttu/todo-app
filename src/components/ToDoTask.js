import React from 'react';

// Style imports
import './ToDoTask.css';

class ToDoTask extends React.Component {
    constructor() {
        super();
        this.state = {
            done: false
        };
    }

    setDone = () => {
        this.setState({
            done: true
        });
    }

    render() {
        let className = 'ToDoTask';
        if (this.state.done) {
            className += ' done';
        }
        return (
            <div className={className} onClick={this.setDone}>
                {this.props.value}
            </div>
        )
    }
}

export default ToDoTask;