# ToDo App

This to-do checklist app is created with [create-react-app](https://create-react-app.dev/).

### How to run

To use this app, you must first install [Node.js](https://nodejs.org/en/). Then you can start the app with `npm start` command.

### What it looks like

![Image of ToDo-app](https://i.imgur.com/b9Peubq.png)